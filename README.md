[![build status](https://gitlab.com/sunbiz/toolkit.librehealth.io/badges/master/build.svg)](https://gitlab.com/sunbiz/toolkit.librehealth.io/commits/master)

---
THIS IS STILL A WORK IN PROGRESS!!

Website for LibreHealth Toolkit - [https://toolkit.librehealth.io](https://toolkit.librehealth.io)
Built using Hugo static site generator. The site is built through GitLab CI and then posted to toolkit.librehealth.io

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Building locally](#building-locally)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Building locally
Ideally, you should just edit and commit the pages on gitlab, so that it can build and deploy the updated site. But if you wanted to build and test locally, you can do the following:
 1. Fork, clone or download this project
 2. [Install](https://gohugo.io/overview/installing/) Hugo
 3. Preview your project: `hugo server` , then point your browser to [localhost:1313/hugo/](https://localhost:1313/hugo/)
 4. Add content
 5. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/overview/introduction/).

## Using the hugo-universal-theme
We are using the hugo-universal-theme for the website.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.
