+++
title = "LibreHealth Projects & Teams"
+++

As an open source umbrella organization, LibreHealth consists of a community of individuals working on multiple projects (software focused) and teams (content or task focused). These sub-projects and teams follow the Project Life Cycle set out in our community governance charter. Some projects will be active, some will be archived when no longer active, and some will be in an "incubation" phases during early ideation stages. For more information, see: https://gitlab.com/librehealth/community-governance/blob/master/GOVERNANCE.md

# Current Full Projects

The following projects are considered as full or "mature" projects as described in the LibreHealth governance polices, and are expected to meet those requirements on an ongoing basis.

## LibreHealth Toolkit

Our Toolkit software serves as the foundational API and data model for many other Health IT applications. It will be frequently (but not always) utilized as an upstream product for buidling other LibreHealth software. Toolkit is a fork of the OpenMRS Platform, and builds on its reliability while improving performance and ease of installation and use.

* Maintainer: Saptarshi Purkayastha
* Official Code Repository: https://gitlab.com/librehealth/lh-toolkit
* Discussion Forum: https://forums.librehealth.io/c/projects/lh-toolkit

## LibreHealth EHR

The LibreHealth EHR applicaiton is a clinically-focused electronic health record (EHR) system designed to be both easy to use "out of the box" and also customizable for use in a variety of health care settings. It builds on the strength of the LibreHealth Toolkit, and adapts many of the proven user experiences built over many years in the OpenEMR system. It is designed to be a modern, easy-to-use experience for health care professionals in their daily work.

* Maintainer: Tony McCormick 
* Official Code Repository: https://github.com/LibreEHR/LibreEHR
* Discussion Forum: https://forums.librehealth.io/c/projects/lh-emr

## LibreHealth Radiology

Our Radiology suite is a customized version of LibreHealth Toolkit with additional tools for radiology and imaging professionals. (Insert more info here.)

* Maintainer: Judy Gichoya 
* Official Code Repository: https://gitlab.com/librehealth/lh-toolkit
* Discussion Forum: https://forums.librehealth.io/c/projects/lh-rad

# Current Full Teams 

Our community also has several teams working to improve our community experience and make it a more useful place for our contributors and customers. Teams follow a similar lifecycle to projects and can also be proposed through the incubation process. Current teams include:

* Documentation (Andy Oram, maintainer)
* Diversity & Inclusion (Jordan Freitas, maintainer)
* Education & University Outreach (TBD, maintainer)
* LibreHealth Steering Committee (volunteer board)

# Incubating Projects & Teams

The following projects are currently in incubation phase and exist at various stages of discussion. More information and discussion about these incubating projects can be found on the LibreHealth Forums.

* LibreHealth Mobile
* Maternal & Child Health

---

> In case you haven't found the answer for your question please feel free to ask in our forums: https://forums.librehealth.io/